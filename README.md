#Pegasus Frontend Documentation

## Introduction
This document is meant as an getting started guide for front-end developers building a front-end package for the pegasus platform. Prior to working with this package you should familiarise yourself with the Pegasus platform from a high level perspective.

#### Role of the front-end package
The pegasus front-end package is meant as an easy way to get started making a skin for the pegasus platform. It consists of two main parts - `core` and `non-branded`. Core is the platform elements that is reused across the different Pegasus sites. Therefor you should not make any changes in the core files. Use them as reference to get a better understanding of the package.

The non-branded package will be your boilerplate for building a new site. Through JSON documents you will be able to change content as well as module configuration. This way you will be able to work with a static dummy site and still keeping things aligned with the pegasus platform. 

#### Static build flow with assemble.io
One of the key tools used in this process is a [assemble.io](http://assemble.io/). If you don't know it already you shold get familiar with it. It will create your static HTML files based on JSON files and handlebars partials wich has many advantages to hardcoding your static HTML files.

## Getting Started
### Environment dependencies
Before getting started please make sure that you have [Node.js](http://nodejs.org/) + [Bower](http://bower.io/) installed and available globally.

### Gaining access
#### Repositories
To get started building a frontend package to the pegasus platform you need access to the repositories hosting the code. To obtain this access please contact [Josef Kjærgaard](mailto:josef@josefkjaergaard.com). 

#### Image content hosting
To be able to run the package you also need FTP acces to `http://pegasus-images.niceurl.co.uk/img`. This will be used for hosting dummy content images while developing to make sure that image delivery is handled exactly the same way as the production envieroment. Please contact [Lee Kelleher](mailto:lee.kelleher@umbrellainc.co.uk).

### Get the code
Once you have recieved access to the repositories you can checkout the core and create a copy of the non-branded package.

You should make sure to checkout the two repositories to a directory structure like this.

	/pegasus
		- /core
		- /[new-non-branded-site-name]
		
#### Checkout core
To checkout core fowllow these steps.

1. 	Navigate to your pegasus-folder in your terminal.
2.	Checkout git repository:


		$ git clone https://josefkjaergaard@bitbucket.org/josefkjaergaard/carlsberg-ff-core.git core

#### Download copy of the non-branded package
1.	Dowload .zip copy from:

	[https://bitbucket.org/josefkjaergaard/carlsberg-ff-non-branded/get/master.zip](https://bitbucket.org/josefkjaergaard/carlsberg-ff-non-branded/get/master.zip)
2. Unzip and place in your `/pegasus` folder
3. Rename unzipped folder to the name of your frontend package. Eg. /pegasus/grimbergen-beer/.

#### Install dependencies
In `/pegasus/core` run:

	$ bower install

In `/pegasus/[new-non-branded-site-name]` run:

	$ npm install

Note need to run the commands with local admin access.

### Create FTP keys
As mentioned above you will need an ftp access to upload content images to test hosting. (This will happen automaticly via a grunt watch configuration)

Place a file `.ftppass` in `/pegasus/[new-non-branded-site-name]` and add your key. Note for the image hosting ftp use `key2`

	{
		"key1": {
		    "username": "[username]",
    		"password": "[password]"
		},
		"key2": {
    		"username": "[username]",
	    	"password": "[password]"
		}
	}



### Run the development task
Now you are ready to run the development task. In `/pegasus/[new-non-branded-site-name]` run:

	grunt

This task will do a number and end in a `watch` state that will keep an eye on changes in the package. Any change will create a new dev-build and update the browsers displaying the local site. 

## Folders
#### Assets
`/assets`
This folder contains static assets used by you css. Note: the css folder contains css that have been compiled through Sass/Compass

The folders `/assets/images/icons` + `/assets/images/icons-2x` contains assets that complass generates sprite-sheet from. Normal and Retina resolution. 

#### Dist dirs
`/dist-develop` + `/dist-production`
These are export dirs and will contain files that are build throug assemble, compass and requireJS optimizer.

#### Partials
`/partials`
Partials placed in this folder will override the core partials. This way you will be able to modify the markup without modifying the core files.

#### Sites
`/sites`
The content pars used to compile the frontend package. Consists of JSON, handlebars templates (.hbs) and images. Implement your dummy content by modifying the the files in this folder.

#### Styles
`/styles`
Containes all sass styling for the skin.

## Core relationship

## Configuration
`sites/global/data/site.json`
This file contains some global configuration for the site you are building. They will both affect the compilation of the makup but also be availibe for the core js modules. Most of these settings will be replaced by a CMS-configurable output when implementing the frontend to Pegasus.

#### Age gate
	"minAge": 18,
	"maxAge": 100,
	"gate": "off",
	"date": {
		"year": 2014,
		"month": 1,
		"day": 19
	},
	
Use the `gate` value to to control the age gate display. Possible values are `birthday`, `yesNo` or `off`. The `date` is the current date being evaluated against - used primarly for testing. 

#### Facebook
	"facebook": {
		"appID": 574603549277253,
		"enabled": true,
		"falconAppID": 3350,
		"promt": {
			"platform": "facebook",
			"permissions": ["birthdate", "likes"]
		}
	},
Use this configuration to setup a dummy app to test the social login.

#### Breakpoints
	"breakpoints": {
		"small": 768,
		"medium": 1024,
		"large": 1200,
		"xlarge": 2999
	},
Used by JS modules. Should be kept in sync with values used by css mediaqueries

#### Highlights module
	"highlights": {
		"small": 1,
		"medium": 2,
		"large": 3,
		"xlarge": 4
	},
	
	
Configuration for the highlights module. Controls how many item to show at the different breakpoints.

#### Grid
		"filterStickyOffset": 75,
		"stretchLargeItems": ["promo", "social"],
		"gutters": {
			"small": 10,
			"medium": 30,
			"large": 30,
			"xlarge": 30,
			"xxlarge": 30
		},
		"small": {
			"large": 12,
			"medium": 12,
			"small": 6
		}, 
		"medium",
		"large"
		...		

`filterStickyOffset` : The offset to apply to the sticky filter-bar of the grid module. Often corresponds to the desktop menu height.

`stretchLargeItems` : Large grid items of these `types` will be streched to 100% of the browser vs. being inside the main container

`gutters`: Define your gutters here. These should be similar to values used in CSS.

`small`, `medium`, `large`, `xlarge` and `xxlarge` : Define how many columns each grid item size should cover in each breakpoint. 

## Page templates and modules
The front-end has three page templates:

-	###Module-list  
	`/sites/global/home.hbs`
	
	A list of sub modules with rich functionality. Usually the homepage will consist of a module-list page with all or some of the modules. Supported modules are:
	
	-	Hero
	-	Highlights
	-	Product slider
	-	Social Grid
-	###Text page
	`/sites/global/about.hbs`
	
	A list of simple content blocks. Text, image, video, forms etc. This template is used for simple pages like about, contact and 404's.
	
-	### Slide page
	`/sites/global/products.hbs`
	
	This template is divided into sections and uses a ''snapping' navigation to always display one section at a time.

## Build task

#### Build
To create the production ready assets (minified CSS + JS) use the `grunt build` task. This will put a clean build into the `dist-production` dir.

#### Build to demo via FTP
If you wish to have a preview of the frontend package available online you can use the `grunt builToDemo` task. To use this task you will have to configure the FTP values in the top of `Gruntfile.js` + add username + password on the ´key1´ value in your local ´.ftppass´.